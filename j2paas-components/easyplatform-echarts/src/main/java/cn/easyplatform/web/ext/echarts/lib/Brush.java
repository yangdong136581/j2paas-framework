/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.BrushStyle;
import cn.easyplatform.web.ext.echarts.lib.support.Rang;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Brush implements Serializable {

    /**
     * 使用在 toolbox 中的按钮,[ default: ['rect', 'polygon', 'keep', 'clear'] ]
     * brush 相关的 toolbox 按钮有：
     * 'rect'：开启矩形选框选择功能。
     * 'polygon'：开启任意形状选框选择功能。
     * 'lineX'：开启横向选择功能。
     * 'lineY'：开启纵向选择功能。
     * 'keep'：切换『单选』和『多选』模式。后者可支持同时画多个选框。前者支持单击清除所有选框。
     * 'clear'：清空所有选框。
     */
    private Object toolbox;
    /**
     * 不同系列间，选中的项可以联动
     */
    private Object brushLink;
    /**
     * 指定哪些 series 可以被刷选，可取值为：
     * 'all': 所有 series
     * 'Array': series index 列表
     * 'number': 某个 series index
     */
    private Object seriesIndex;
    /**
     * 指定哪些 geo 可以被刷选
     */
    private Object geoIndex;
    /**
     * 指定哪些 xAxisIndex 可以被刷选
     */
    private Object xAxisIndex;
    /**
     * 指定哪些 yAxisIndex 可以被刷选
     */
    private Object yAxisIndex;
    /**
     * 默认的刷子类型。
     * 'rect'：矩形选框。
     * 'polygon'：任意形状选框。
     * 'lineX'：横向选择。
     * 'lineY'：纵向选择。
     */
    private String brushType;
    /**
     * 默认的刷子的模式
     */
    private String brushMode;
    /**
     * 已经选好的选框是否可以被调整形状或平移
     */
    private Boolean transformable;
    /**
     * 选框的默认样式
     */
    private BrushStyle brushStyle;
    /**
     * 默认情况，刷选或者移动选区的时候，会不断得发 brushSelected 事件，从而告诉外界选中的内容
     * \throttleType 取值可以是：
     * 'debounce'：表示只有停止动作了（即一段时间没有操作了），才会触发事件。时间阈值由 brush.throttleDelay 指定。
     * 'fixRate'：表示按照一定的频率触发时间，时间间隔由 brush.throttleDelay 指定。
     */
    private String throttleType;
    /**
     * 默认为 0 表示不开启 throttle
     */
    private Integer throttleDelay;
    /**
     * 在 brush.brushMode 为 'single' 的情况下，是否支持『单击清除所有选框』
     */
    private Boolean removeOnClick;
    /**
     * 定义 在选中范围中 的视觉元素
     */
    private Rang inBrush;
    /**
     * 定义 在选中范围外 的视觉元素
     */
    private Rang outOfBrush;
    /**
     * brush 选框的 z-index。当有和不想管组件有不正确的重叠时，可以进行调整。
     */
    private Integer z;

    public Integer z() {
        return z;
    }

    public Brush z(Integer z) {
        this.z = z;
        return this;
    }

    public Object toolbox() {
        return toolbox;
    }

    public Brush toolbox(Object toolbox) {
        this.toolbox = toolbox;
        return this;
    }

    public Object brushLink() {
        return brushLink;
    }

    public Brush brushLink(Object brushLink) {
        this.brushLink = brushLink;
        return this;
    }

    public Object seriesIndex() {
        return seriesIndex;
    }

    public Brush seriesIndex(Object seriesIndex) {
        this.seriesIndex = seriesIndex;
        return this;
    }

    public Object geoIndex() {
        return geoIndex;
    }

    public Brush geoIndex(Object geoIndex) {
        this.geoIndex = geoIndex;
        return this;
    }

    public Object xAxisIndex() {
        return xAxisIndex;
    }

    public Brush xAxisIndex(Object... xAxisIndex) {
        if (xAxisIndex.length == 1)
            this.xAxisIndex = xAxisIndex[0];
        else
            this.xAxisIndex = xAxisIndex;
        return this;
    }

    public Object yAxisIndex() {
        return yAxisIndex;
    }

    public Brush yAxisIndex(Object... yAxisIndex) {
        if (yAxisIndex.length == 1)
            this.yAxisIndex = yAxisIndex[0];
        else
            this.yAxisIndex = yAxisIndex;
        return this;
    }

    public String brushType() {
        return brushType;
    }

    public Brush brushType(String brushType) {
        this.brushType = brushType;
        return this;
    }

    public String brushMode() {
        return brushMode;
    }

    public Brush brushMode(String brushMode) {
        this.brushMode = brushMode;
        return this;
    }

    public Boolean transformable() {
        return transformable;
    }

    public Brush transformable(Boolean transformable) {
        this.transformable = transformable;
        return this;
    }

    public BrushStyle brushStyle() {
        return brushStyle;
    }

    public Brush brushStyle(BrushStyle brushStyle) {
        this.brushStyle = brushStyle;
        return this;
    }

    public String throttleType() {
        return throttleType;
    }

    public Brush throttleType(String throttleType) {
        this.throttleType = throttleType;
        return this;
    }

    public Integer throttleDelay() {
        return throttleDelay;
    }

    public Brush throttleDelay(Integer throttleDelay) {
        this.throttleDelay = throttleDelay;
        return this;
    }

    public Boolean removeOnClick() {
        return removeOnClick;
    }

    public Brush removeOnClick(Boolean removeOnClick) {
        this.removeOnClick = removeOnClick;
        return this;
    }

    public Rang inBrush() {
        if (inBrush == null)
            inBrush = new Rang();
        return this.inBrush;
    }

    public Brush inRange(Rang inBrush) {
        this.inBrush = inBrush;
        return this;
    }

    public Rang outOfBrush() {
        if (outOfBrush == null)
            outOfBrush = new Rang();
        return this.outOfBrush;
    }

    public Brush outOfRange(Rang outOfBrush) {
        this.outOfBrush = outOfBrush;
        return this;
    }

    public Object getToolbox() {
        return toolbox;
    }

    public void setToolbox(Object toolbox) {
        this.toolbox = toolbox;
    }

    public Object getBrushLink() {
        return brushLink;
    }

    public void setBrushLink(Object brushLink) {
        this.brushLink = brushLink;
    }

    public Object getSeriesIndex() {
        return seriesIndex;
    }

    public void setSeriesIndex(Object seriesIndex) {
        this.seriesIndex = seriesIndex;
    }

    public Object getGeoIndex() {
        return geoIndex;
    }

    public void setGeoIndex(Object geoIndex) {
        this.geoIndex = geoIndex;
    }

    public Object getxAxisIndex() {
        return xAxisIndex;
    }

    public void setxAxisIndex(Object xAxisIndex) {
        this.xAxisIndex = xAxisIndex;
    }

    public Object getyAxisIndex() {
        return yAxisIndex;
    }

    public void setyAxisIndex(Object yAxisIndex) {
        this.yAxisIndex = yAxisIndex;
    }

    public String getBrushType() {
        return brushType;
    }

    public void setBrushType(String brushType) {
        this.brushType = brushType;
    }

    public String getBrushMode() {
        return brushMode;
    }

    public void setBrushMode(String brushMode) {
        this.brushMode = brushMode;
    }

    public Boolean getTransformable() {
        return transformable;
    }

    public void setTransformable(Boolean transformable) {
        this.transformable = transformable;
    }

    public BrushStyle getBrushStyle() {
        return brushStyle;
    }

    public void setBrushStyle(BrushStyle brushStyle) {
        this.brushStyle = brushStyle;
    }

    public String getThrottleType() {
        return throttleType;
    }

    public void setThrottleType(String throttleType) {
        this.throttleType = throttleType;
    }

    public Integer getThrottleDelay() {
        return throttleDelay;
    }

    public void setThrottleDelay(Integer throttleDelay) {
        this.throttleDelay = throttleDelay;
    }

    public Boolean getRemoveOnClick() {
        return removeOnClick;
    }

    public void setRemoveOnClick(Boolean removeOnClick) {
        this.removeOnClick = removeOnClick;
    }

    public Rang getInBrush() {
        return inBrush;
    }

    public void setInBrush(Rang inBrush) {
        this.inBrush = inBrush;
    }

    public Rang getOutOfBrush() {
        return outOfBrush;
    }

    public void setOutOfBrush(Rang outOfBrush) {
        this.outOfBrush = outOfBrush;
    }
}
