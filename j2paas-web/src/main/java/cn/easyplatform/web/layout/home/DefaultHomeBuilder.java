/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.layout.home;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.messages.request.BeginRequestMessage;
import cn.easyplatform.messages.vos.AbstractPageVo;
import cn.easyplatform.messages.vos.AuthorizationVo;
import cn.easyplatform.messages.vos.RoleVo;
import cn.easyplatform.messages.vos.TaskVo;
import cn.easyplatform.spi.service.TaskService;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.web.layout.IHomeBuilder;
import cn.easyplatform.web.layout.IMainTaskBuilder;
import cn.easyplatform.web.layout.LayoutManagerFactory;
import cn.easyplatform.web.service.ServiceLocator;
import cn.easyplatform.web.task.BackendException;
import cn.easyplatform.web.task.MainTaskSupport;
import org.zkoss.util.resource.Labels;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Panel;
import org.zkoss.zul.Panelchildren;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 * @deprecated
 */
public class DefaultHomeBuilder implements IHomeBuilder {

    @Override
    public void fill(Component comp, AuthorizationVo av) {
        if (av.getRoles() == null)
            return;
        Iterator<Component> itr = comp.queryAll("panel").iterator();
        List<Component> comps = new ArrayList<Component>();
        String defaultRoleId = null;
        if (av.getRoles().isEmpty())
            defaultRoleId = "default";
        else
            defaultRoleId = av.getRoles().get(0).getId();
        while (itr.hasNext())
            comps.add(itr.next());
        itr = comps.iterator();
        while (itr.hasNext()) {
            Panel container = (Panel) itr.next();
            if (container.getAttribute("roles") != null) {
                String[] roles = ((String) container.getAttribute("roles"))
                        .split(",");
                boolean isRole = false;
                for (String role : roles) {
                    for (RoleVo rv : av.getRoles()) {
                        if (rv.getId().equals(role)) {
                            defaultRoleId = role;
                            isRole = true;
                            break;
                        }
                    }
                    if (isRole)
                        break;
                }
                if (!isRole) {
                    container.detach();
                    container = null;
                    itr.remove();
                    continue;
                }
            }
            Panelchildren pc = new Panelchildren();
            container.appendChild(pc);
            String id = container.getId();
            if (id.startsWith("#{") && id.endsWith("}")) {
                String taskId = container.getId().substring(2, id.length() - 1);
                TaskService mtc = ServiceLocator
                        .lookup(TaskService.class);
                TaskVo tv = new TaskVo(taskId);
                tv.setRoleId(defaultRoleId);
                BeginRequestMessage req = new BeginRequestMessage(tv);
                IResponseMessage<?> resp = mtc.begin(req);
                if (resp.isSuccess()) {
                    MainTaskSupport builder = null;
                    try {
                        AbstractPageVo pv = (AbstractPageVo) resp.getBody();
                        String showTitle = (String) container
                                .getAttribute("showTitle");
                        if (showTitle == null
                                || showTitle.equalsIgnoreCase("true"))
                            container.setTitle(pv.getTitile());
                        builder = (MainTaskSupport) LayoutManagerFactory
                                .createLayoutManager()
                                .getMainTaskBuilder(container,
                                        tv.getId(), pv);
                        ((IMainTaskBuilder) builder).build();
                    } catch (EasyPlatformWithLabelKeyException ex) {
                        builder.close(false);
                        showError(pc,
                                Labels.getLabel(ex.getMessage(), ex.getArgs()));
                    } catch (BackendException ex) {
                        builder.close(false);
                        showError(pc, ex.getMsg().getCode() + ":"
                                + ex.getMsg().getBody());
                    } catch (Exception ex) {
                        builder.close(false);
                        showError(container,
                                ex.getMessage());
                    }
                } else {
                    showError(pc, resp.getCode() + ":" + resp.getBody());
                }
            }
        }
        comps = null;
    }

    private void showError(Component parent, String msg) {
        Div div = new Div();
        div.setClass("alert alert-danger");
        div.setStyle("margin-top:30px");
        Label label = new Label();
        label.setValue(msg);
        label.setParent(div);
        div.setParent(parent);
    }
}
