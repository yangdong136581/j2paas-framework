/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.admin;

import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.lang.Strings;
import cn.easyplatform.log.ConsoleType;
import cn.easyplatform.log.LogManager;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.IResponseMessage;
import org.apache.logging.log4j.Level;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;

/**
 * @Author: <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @Description:
 * @Since: 2.0.0 <br/>
 * @Date: Created in 2019/11/7 17:13
 * @Modified By:
 */
public class ConsoleCmd extends AbstractCommand<SimpleRequestMessage> {

    private final static Logger log = LoggerFactory.getLogger(ConsoleCmd.class);

    public ConsoleCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        Object[] data = (Object[]) req.getBody();
        if (log.isInfoEnabled())
            log.info("Console set {}.", Arrays.toString(data));
        String filter = (String) data[0];
        if (data[1] instanceof Boolean) {
            Boolean enabled = (Boolean) data[1];
            if (enabled) {
                if (Strings.isBlank(filter))
                    LogManager.startConsole(ConsoleType.APP, Level.DEBUG, cc, cc.getEnv().getId());
                else if (cc.getProjectService() == null)
                    LogManager.startConsole(ConsoleType.APP, Level.DEBUG, cc, filter);
                else
                    LogManager.startConsole(ConsoleType.USER, Level.DEBUG, cc, filter);
            } else
                LogManager.stopConsole(cc.getUser());
        } else {//设置日志级别
            LogManager.stopConsole(cc.getUser());
            String name = (String) data[1];
            if (Strings.isBlank(filter))
                LogManager.startConsole(ConsoleType.APP, Level.getLevel(name), cc, cc.getEnv().getId());
            else if (cc.getProjectService() == null)
                LogManager.startConsole(ConsoleType.APP, Level.getLevel(name), cc, filter);
            else
                LogManager.startConsole(ConsoleType.USER, Level.getLevel(name), cc, filter);
        }
        return new SimpleResponseMessage();
    }
}
