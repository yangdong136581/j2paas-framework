/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.api;


import cn.easyplatform.entities.BaseEntity;

import javax.xml.bind.annotation.*;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"inputs", "refId", "anonymous", "outputs", "runAsNodes", "runAsRoles",
        "runAsUsers","fieldsDetail"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "api")
public class ApiBean extends BaseEntity {

    @XmlElementWrapper(name = "inputs")
    @XmlElement(name = "input")
    private List<Input> inputs;

    @XmlElementWrapper(name = "outputs")
    @XmlElement(name = "output")
    private List<Output> outputs;

    @XmlElement(required = true)
    private String refId;// 引用参数的id（功能、列表、逻辑、报表,SELECT语句）

    @XmlElement
    private boolean anonymous;//是否允许匿名访问

    @XmlElement
    private String runAsNodes;

    @XmlElement
    private String runAsRoles;

    @XmlElement
    private String runAsUsers;

    @XmlElement
    private String fieldsDetail;

    public String getFieldsDetail() {
        return fieldsDetail;
    }

    public void setFieldsDetail(String fieldsDetail) {
        this.fieldsDetail = fieldsDetail;
    }

    public boolean isAnonymous() {
        return anonymous;
    }

    public void setAnonymous(boolean anonymous) {
        this.anonymous = anonymous;
    }

    public List<Input> getInputs() {
        return inputs;
    }

    public void setInputs(List<Input> inputs) {
        this.inputs = inputs;
    }

    public List<Output> getOutputs() {
        return outputs;
    }

    public void setOutputs(List<Output> outputs) {
        this.outputs = outputs;
    }

    public String getRefId() {
        return refId;
    }

    public void setRefId(String refId) {
        this.refId = refId;
    }

    public String getRunAsNodes() {
        return runAsNodes;
    }

    public void setRunAsNodes(String runAsNodes) {
        this.runAsNodes = runAsNodes;
    }

    public String getRunAsRoles() {
        return runAsRoles;
    }

    public void setRunAsRoles(String runAsRoles) {
        this.runAsRoles = runAsRoles;
    }

    public String getRunAsUsers() {
        return runAsUsers;
    }

    public void setRunAsUsers(String runAsUsers) {
        this.runAsUsers = runAsUsers;
    }
}
