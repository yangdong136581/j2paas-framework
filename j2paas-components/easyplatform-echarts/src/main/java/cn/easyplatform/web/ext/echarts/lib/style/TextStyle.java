/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import cn.easyplatform.web.ext.echarts.lib.type.FontWeight;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * 文字样式
 *
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TextStyle implements Serializable {

    private static final long serialVersionUID = 5748410562515851843L;

    /**
     * 颜色
     */
    private String color;
    /**
     * 样式，可选为：'normal' | 'italic' | 'oblique'
     */
    private String fontStyle;
    /**
     * 粗细，可选为：'normal' | 'bold' | 'bolder' | 'lighter' | 100 | 200 |... | 900
     *
     * @see FontWeight
     */
    private Object fontWeight;
    /**
     * 字体系列
     */
    private String fontFamily;
    /**
     * 字号 ，单位px
     */
    private Object fontSize;
    /**
     * 行高,rich 中如果没有设置 lineHeight，则会取父层级的 lineHeight
     */
    private Object lineHeight;

    /**
     * 文字块的宽度。一般不用指定，不指定则自动是文字的宽度。在想做表格项或者使用图片（参见 backgroundColor）时，可能会使用它
     */
    private Object width;

    /**
     * 文字块的高度。一般不用指定，不指定则自动是文字的高度。在使用图片（参见 backgroundColor）时，可能会使用它。
     */
    private Object height;
    /**
     * 文字本身的描边颜色。
     */
    private String textBorderColor;

    /**
     * 文字本身的描边宽度。
     */
    private Integer textBorderWidth;

    /**
     * 折线主线(IE8+)有效，阴影色彩，支持rgba
     */
    private String textShadowColor;
    /**
     * 默认值5，折线主线(IE8+)有效，阴影模糊度，大于0有效
     */
    private Integer textShadowBlur;

    /**
     * 文字本身的阴影 X 偏移。
     */
    private Integer textShadowOffsetX;

    /**
     * 文字本身的阴影 Y 偏移。
     */
    private Integer textShadowOffsetY;
    /**
     * 在 rich 里面，可以自定义富文本样式。利用富文本样式，可以在标签中做出非常丰富的效果
     */
    private Map<String, TextStyle> rich;

    public Map<String, TextStyle> rich() {
        if (rich == null)
            rich = new HashMap<>();
        return this.rich;
    }

    public TextStyle rich(String name, TextStyle style) {
        if (rich == null)
            rich = new HashMap<>();
        rich.put(name, style);
        return this;
    }

    public Integer textShadowOffsetY() {
        return this.textShadowOffsetY;
    }

    public TextStyle textShadowOffsetY(Integer textShadowOffsetY) {
        this.textShadowOffsetY = textShadowOffsetY;
        return this;
    }

    public Integer textShadowOffsetX() {
        return this.textShadowOffsetX;
    }

    public TextStyle textShadowOffsetX(Integer textShadowOffsetX) {
        this.textShadowOffsetX = textShadowOffsetX;
        return this;
    }

    public Integer textBorderWidth() {
        return this.textBorderWidth;
    }

    public TextStyle textBorderWidth(Integer textBorderWidth) {
        this.textBorderWidth = textBorderWidth;
        return this;
    }

    public String textBorderColor() {
        return this.textBorderColor;
    }

    public TextStyle textBorderColor(String textBorderColor) {
        this.textBorderColor = textBorderColor;
        return this;
    }

    public Object height() {
        return this.height;
    }

    public TextStyle height(Object height) {
        this.height = height;
        return this;
    }

    public Object width() {
        return this.width;
    }

    public TextStyle width(Object width) {
        this.width = width;
        return this;
    }

    public Object lineHeight() {
        return this.lineHeight;
    }

    public TextStyle lineHeight(Object lineHeight) {
        this.lineHeight = lineHeight;
        return this;
    }

    /**
     * 获取shadowColor值
     */
    public String textShadowColor() {
        return this.textShadowColor;
    }

    /**
     * 设置shadowColor值
     *
     * @param textShadowColor
     */
    public TextStyle textShadowColor(String textShadowColor) {
        this.textShadowColor = textShadowColor;
        return this;
    }

    /**
     * 获取shadowBlur值
     */
    public Integer textShadowBlur() {
        return this.textShadowBlur;
    }

    /**
     * 设置shadowBlur值
     *
     * @param textShadowBlur
     */
    public TextStyle textShadowBlur(Integer textShadowBlur) {
        this.textShadowBlur = textShadowBlur;
        return this;
    }

    /**
     * 获取color值
     */
    public String color() {
        return this.color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public TextStyle color(String color) {
        this.color = color;
        return this;
    }

    /**
     * 获取fontSize值
     */
    public Object fontSize() {
        return this.fontSize;
    }

    /**
     * 设置fontSize值
     *
     * @param fontSize
     */
    public TextStyle fontSize(Object fontSize) {
        this.fontSize = fontSize;
        return this;
    }

    /**
     * 获取fontFamily值
     */
    public String fontFamily() {
        return this.fontFamily;
    }

    /**
     * 设置fontFamily值
     *
     * @param fontFamily
     */
    public TextStyle fontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
        return this;
    }

    /**
     * 获取fontStyle值
     */
    public String fontStyle() {
        return this.fontStyle;
    }

    /**
     * 设置fontStyle值
     *
     * @param fontStyle
     */
    public TextStyle fontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
        return this;
    }

    /**
     * 获取fontWeight值
     */
    public Object fontWeight() {
        return this.fontWeight;
    }

    /**
     * 设置fontWeight值
     *
     * @param fontWeight
     */
    public TextStyle fontWeight(Object fontWeight) {
        this.fontWeight = fontWeight;
        return this;
    }

    /**
     * 获取color值
     */
    public String getColor() {
        return color;
    }

    /**
     * 设置color值
     *
     * @param color
     */
    public void setColor(String color) {
        this.color = color;
    }

    /**
     * 获取fontSize值
     */
    public Object getFontSize() {
        return fontSize;
    }

    /**
     * 设置fontSize值
     *
     * @param fontSize
     */
    public void setFontSize(Object fontSize) {
        this.fontSize = fontSize;
    }

    /**
     * 获取fontFamily值
     */
    public String getFontFamily() {
        return fontFamily;
    }

    /**
     * 设置fontFamily值
     *
     * @param fontFamily
     */
    public void setFontFamily(String fontFamily) {
        this.fontFamily = fontFamily;
    }

    /**
     * 获取fontStyle值
     */
    public String getFontStyle() {
        return fontStyle;
    }

    /**
     * 设置fontStyle值
     *
     * @param fontStyle
     */
    public void setFontStyle(String fontStyle) {
        this.fontStyle = fontStyle;
    }

    /**
     * 获取fontWeight值
     */
    public Object getFontWeight() {
        return fontWeight;
    }

    /**
     * 设置fontWeight值
     *
     * @param fontWeight
     */
    public void setFontWeight(Object fontWeight) {
        this.fontWeight = fontWeight;
    }

    public Object getLineHeight() {
        return lineHeight;
    }

    public void setLineHeight(Object lineHeight) {
        this.lineHeight = lineHeight;
    }

    public Object getWidth() {
        return width;
    }

    public void setWidth(Object width) {
        this.width = width;
    }

    public Object getHeight() {
        return height;
    }

    public void setHeight(Object height) {
        this.height = height;
    }

    public String getTextBorderColor() {
        return textBorderColor;
    }

    public void setTextBorderColor(String textBorderColor) {
        this.textBorderColor = textBorderColor;
    }

    public Integer getTextBorderWidth() {
        return textBorderWidth;
    }

    public void setTextBorderWidth(Integer textBorderWidth) {
        this.textBorderWidth = textBorderWidth;
    }

    public String getTextShadowColor() {
        return textShadowColor;
    }

    public void setTextShadowColor(String textShadowColor) {
        this.textShadowColor = textShadowColor;
    }

    public Integer getTextShadowBlur() {
        return textShadowBlur;
    }

    public void setTextShadowBlur(Integer textShadowBlur) {
        this.textShadowBlur = textShadowBlur;
    }

    public Integer getTextShadowOffsetX() {
        return textShadowOffsetX;
    }

    public void setTextShadowOffsetX(Integer textShadowOffsetX) {
        this.textShadowOffsetX = textShadowOffsetX;
    }

    public Integer getTextShadowOffsetY() {
        return textShadowOffsetY;
    }

    public void setTextShadowOffsetY(Integer textShadowOffsetY) {
        this.textShadowOffsetY = textShadowOffsetY;
    }

    public Map<String, TextStyle> getRich() {
        return rich;
    }

    public void setRich(Map<String, TextStyle> rich) {
        this.rich = rich;
    }
}
