/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.exporter.util;

import org.zkoss.zul.GroupsModelArray;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class GroupsModelArrayAdapter<D, H, F, E> extends GroupsModelArray<D, H, F, E>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public GroupsModelArrayAdapter(D[] data, Comparator<D> cmpr) {
		super(data, cmpr);
	}
	
	private Collection< Collection<D> > _d;
	
	public Collection< Collection<D> > getData() {
		if (_d == null) {
			_d = new ArrayList<Collection<D>>(_data.length);
			for (D[] d : _data) {
				_d.add(Arrays.asList(d));
			}
		}
		return _d;
	}
}
