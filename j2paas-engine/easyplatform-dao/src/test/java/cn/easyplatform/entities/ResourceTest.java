/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities;

import cn.easyplatform.entities.beans.ResourceBean;
import cn.easyplatform.entities.transform.TransformerFactory;
import cn.easyplatform.lang.Dumps;
import cn.easyplatform.type.EntityType;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ResourceTest {
	@Test
	public void testJava2Xml() {
		// 数据库连接池
		ResourceBean rb = new ResourceBean();
		rb.setType(EntityType.DATASOURCE.getName());
		Map<String, String> props = new HashMap<String, String>();
		props.put("jdbcUrl", "jdbc:derby:easyplatform-entities;create=false");
		props.put("username", "grantie");
		props.put("password", "1qazxsw2");
		props.put("driverClassName", "org.apache.derby.jdbc.EmbeddedDriver");
		props.put("initialSize", "5");
		props.put("maxActive", "20");
		props.put("minIdle", "5");
		props.put("maxWait", "-1");
		props.put("poolPreparedStatements", "true");
		props.put("maxOpenPreparedStatements", "100");
		props.put("validationQuery", "select 1");
		rb.setProps(props);
		toFile(rb, "datasource.xml");
	}

	private void toFile(ResourceBean rb, String name) {
		try {
			URL url = getClass().getResource(
					"/cn/easyplatform/entities/beans/");
			File file = new File(url.getFile() + name);
			TransformerFactory.newInstance().transformToXml(rb,
					new FileOutputStream(file));
			InputStream is = new FileInputStream(file);
			ResourceBean lb = TransformerFactory.newInstance()
					.transformFromXml(ResourceBean.class, is);
			System.out.println(Dumps.obj(lb));
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
