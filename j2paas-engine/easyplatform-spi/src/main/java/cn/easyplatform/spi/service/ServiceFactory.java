/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.spi.service;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ServiceFactory {

    /**
     * 注册本地（RPC)连接服务
     *
     * @param ifc
     * @param impl
     */
    void registry(Class<?> ifc, Class<?> impl);

    /**
     * 查找连接服务
     *
     * @param clazz
     * @param <T>
     * @return
     */
    <T> T lockup(Class<T> clazz);

    /**
     * 销毁注册的服务
     */
    void destory();
}
