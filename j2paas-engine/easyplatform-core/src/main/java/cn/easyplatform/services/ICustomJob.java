/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.services;

import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.entities.beans.LogicBean;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface ICustomJob {

    /**
     * 执行任务
     *
     * @param entity
     * @param time
     * @param fields
     */
    long schedule(LogicBean entity, Object time, List<FieldDo> fields);

    /**
     * 取消任务
     *
     * @param id
     */
    void stop(long id);

    /**
     * 更改任务的执行时间
     *
     * @param id
     * @param time
     */
    boolean modify(long id, Object time);

    /**
     * 获取等待执行的任务
     *
     * @return
     */
    Collection<Map<String, Object>> getJobs();

    /**
     * 开始服务
     */
    void start();

    /**
     * 停止服务
     */
    void stop();
}
