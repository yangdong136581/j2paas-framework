/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.contexts;

import cn.easyplatform.cfg.EngineConfiguration;
import cn.easyplatform.spi.extension.ProjectContext;

import java.util.Properties;
import java.util.*;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ApplicationContext implements cn.easyplatform.spi.extension.ApplicationContext {

    private EngineConfiguration engineConfiguration;

    private Map<String, String> configs;

    public ApplicationContext(EngineConfiguration engineConfiguration, Properties properties) {
        this.engineConfiguration = engineConfiguration;
        configs = new HashMap<>(properties.size());
        for (Object o : properties.keySet()) {
            String key = o.toString();
            configs.put(key, properties.getProperty(key));
        }
    }

    @Override
    public String getConfig(String name) {
        return configs.get(name);
    }

    @Override
    public ProjectContext getProjectContext(String projectId) {
        return (ProjectContext) engineConfiguration.getService(projectId);
    }

    @Override
    public String getApplicationConfigValue(String group, String name) {
        return engineConfiguration.getPlatformAttribute(group, name);
    }

    @Override
    public Map<String, String> getApplicationConfig(String name) {
        return engineConfiguration.getAttributes(name);
    }

    @Override
    public Set<String> keys() {
        return configs.keySet();
    }
}
