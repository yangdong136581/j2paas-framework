{
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: 'data'
    },
    yAxis: {
        type: 'value'
    },
    series: [{
        data: 'data',
        type: 'line',
        areaStyle: {}
    }]
}