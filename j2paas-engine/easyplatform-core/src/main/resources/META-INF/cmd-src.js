importPackage(java.util, java.lang);
function cache() {
    if (arguments.length == 1)
        return $.cache(arguments[0], null);
    else if (arguments.length == 2)
        return $.cache(arguments[0], arguments[1]);
}
function exists() {
    if (arguments.length == 1)
        return $.exists(arguments[0], '$');
    else if (arguments.length == 2)
        return $.exists(arguments[0], arguments[1]);
}
function createVariable() {
    if (arguments.length == 2)
        return $.createVariable(arguments[0], arguments[1], null, 0);
    else if (arguments.length == 3)
        return $.createVariable(arguments[0], arguments[1], arguments[2], 0);
    else if (arguments.length == 4)
        return $.createVariable(arguments[0], arguments[1], arguments[2], arguments[3]);
}
function getValue() {
    if (arguments.length == 1)
        $.getVariable(arguments[0], '$', '');
    else if (arguments.length == 2)
        $.getVariable(arguments[0], arguments[1], '');
    else if (arguments.length == 3)
        $.getVariable(arguments[0], arguments[1], arguments[2]);
}
function setValue() {
    if (arguments.length == 1)
        $.setVariable(arguments[0], null, '$', '');
    else if (arguments.length == 2)
        $.setVariable(arguments[0], arguments[1], '$', '');
    else if (arguments.length == 3)
        $.setVariable(arguments[0], arguments[1], arguments[2], '');
    else if (arguments.length == 4)
        $.setVariable(arguments[0], arguments[1], arguments[2], arguments[3]);
}
function get$(n) {
    return $.get$(n);
}
function set$(n, v) {
    $.set$(n, v);
}
function get0$(n) {
    return $.get0$(n);
}
function set0$(n, v) {
    $.set0$(n, v);
}
function list$(n, sel) {
    return $.list$(n, sel);
}
function load() {
    if (arguments.length == 0)
        $.load(null, null);
    else if (arguments.length == 1)
        $.load(arguments[0], null);
    else if (arguments.length == 2)
        $.load(arguments[0], arguments[1]);
}
function loadFrom() {
    if (arguments.length == 1)
        $.loadFrom(arguments[0], null);
    else if (arguments.length == 2)
        $.loadFrom(arguments[0], arguments[1]);
    else if (arguments.length == 3)
        $.loadFrom(arguments[0], arguments[1], arguments[2]);
}
function getSerial() {
    if (arguments.length == 1)
        return $.getSerial(arguments[0], 0);
    else if (arguments.length == 2)
        return $.getSerial(arguments[0], arguments[1]);
}
function resetSerial(name) {
    $.resetSerial(name);
}
function deleteSerial(name) {
    $.deleteSerial(name);
}
function invoke() {
    var args = new Array();
    for (var i = 1; i < arguments.length; i++)
        args[i - 1] = arguments[i];
    $.invoke(arguments[0], args);
}
function go() {
    if (arguments.length == 1)
        $.go(arguments[0], null);
    else if (arguments.length == 2)
        $.go(arguments[0], arguments[1]);
}
function appendTask() {
    if (arguments.length == 1)
        $.appendTask(arguments[0], null);
    else if (arguments.length == 2)
        $.appendTask(arguments[0], arguments[1]);
}
function executeBatch() {
    if (arguments.length == 0)
        $.executeBatch(true);
    else if (arguments.length == 1)
        $.executeBatch(arguments[0]);
}
function debug(obj) {
    $.debug(obj);
}
function isEmpty(obj) {
    return $.isEmpty(obj);
}
function isEquals(o1, o2) {
    return $.isEquals(o1, o2);
}
function isDefine(name) {
    return $.isDefine(name);
}
function log(msg) {
    $.log(msg);
}
function getObject(name) {
    return $.getObject(name);
}
function i18n(name) {
    return $.i18n(name);
}