/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.addon;

import java.io.Serializable;

import cn.easyplatform.type.FieldVo;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class UpdateVo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String dsId;
	
	private String sql;
	
	private FieldVo[] params;

	/**
	 * @param dsId
	 * @param sql
	 * @param params
	 */
	public UpdateVo(String dsId, String sql, FieldVo[] params) {
		this.dsId = dsId;
		this.sql = sql;
		this.params = params;
	}

	public String getDsId() {
		return dsId;
	}

	public String getSql() {
		return sql;
	}

	public FieldVo[] getParams() {
		return params;
	}
	
	
}
