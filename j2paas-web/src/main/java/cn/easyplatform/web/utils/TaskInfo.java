/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.utils;

import cn.easyplatform.lang.Lang;
import cn.easyplatform.lang.Strings;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TaskInfo implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 功能执行id
     */
    private String id;

    /**
     * 标题
     */
    private String title;

    /**
     * 图标
     */
    private String image;

    /**
     * 功能的id
     */
    private String taskId;

    /**
     * 触发事件组件名称： 菜单:null 页面按钮：button.label 表格:header.name
     */
    private String name;

    /**
     * 表格id
     */
    private String listId;

    /**
     * 对应的记录key
     */
    private Object[] keys;

    /**
     * 父功能执行id
     */
    private String parentId;

    /**
     * 所属的子功能列表
     */
    private List<TaskInfo> children;

    public TaskInfo(String taskId) {
        this.taskId = taskId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getTaskId() {
        return taskId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getListId() {
        return listId;
    }

    public void setListId(String listId) {
        this.listId = listId;
    }

    public Object[] getKeys() {
        return keys;
    }

    public void setKeys(Object[] keys) {
        this.keys = keys;
    }

    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    public List<TaskInfo> getChildren() {
        return children;
    }

    public void addChild(TaskInfo child) {
        if (children == null)
            children = new ArrayList<TaskInfo>();
        children.add(child);
    }

    @Override
    public int hashCode() {
        int iTotal = taskId.hashCode();
        if (!Strings.isBlank(name))
            iTotal += name.hashCode();
        if (!Strings.isBlank(listId))
            iTotal += listId.hashCode();
        if (keys != null && keys.length > 0) {
            for (Object key : keys) {
                if (key != null)
                    iTotal += key.hashCode();
            }
        }
        return iTotal;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof TaskInfo))
            return false;
        TaskInfo other = (TaskInfo) obj;
        return Lang.equals(taskId, other.taskId)
                && Lang.equals(name, other.name)
                && Lang.equals(listId, other.listId)
                && Lang.equals(keys, other.keys);
    }

}
