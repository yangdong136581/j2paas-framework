/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.datalist;

import java.util.Collection;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class ListRecurVo extends ListVo {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Collection<ListQueryParameterVo> panelQueryInfo;

	private int startPageNo;
	
	public ListRecurVo(String name) {
		super(name);
	}

	public Collection<ListQueryParameterVo> getPanelQueryInfo() {
		return panelQueryInfo;
	}

	public void setPanelQueryInfo(Collection<ListQueryParameterVo> panelQueryInfo) {
		this.panelQueryInfo = panelQueryInfo;
	}

	public int getStartPageNo() {
		return startPageNo;
	}

	public void setStartPageNo(int startPageNo) {
		this.startPageNo = startPageNo;
	}

}
