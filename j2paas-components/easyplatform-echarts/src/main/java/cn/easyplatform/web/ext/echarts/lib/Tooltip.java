/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib;

import cn.easyplatform.web.ext.echarts.lib.style.TextStyle;
import cn.easyplatform.web.ext.echarts.lib.type.Trigger;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Tooltip implements Serializable {
    /**
     * 是否显示
     */
    private Boolean show;
    /**
     * tooltip主体内容显示策略，只需tooltip触发事件或显示axisPointer而不需要显示内容时可配置该项为falase，
     * 可选为：true（显示） | false（隐藏）
     */
    private Boolean showContent;
    /**
     * 触发类型，默认数据触发，见下图，可选为：'item' | 'axis'
     *
     * @see Trigger
     */
    private String trigger;
    /**
     * 提示框触发的条件
     */
    private String triggerOn;
    /**
     * 是否永远显示提示框内容，默认情况下在移出可触发提示框区域后 一定时间 后隐藏，设置为 true 可以保证一直显示提示框内容
     */
    private Boolean alwaysShowContent;
    /**
     * 默认20，显示延迟，添加显示延迟可以避免频繁切换，特别是在详情内容需要异步获取的场景，单位ms
     */
    private Integer showDelay;
    /**
     * 默认100，隐藏延迟，单位ms
     */
    private Integer hideDelay;
    /**
     * 2.1.9新增属性，默认true，鼠标是否可进入详情气泡中，默认为false，如需详情内交互，如添加链接，按钮，可设置为true。
     */
    private Boolean enterable;
    /**
     * 提示框浮层的位置，默认不设置时位置会跟随鼠标的位置
     */
    private Object position;
    /**
     * 是否将 tooltip 框限制在图表的区域内
     */
    private Boolean confine;
    /**
     * 动画变换时长，单位s，如果你希望tooltip的跟随实时响应，showDelay设置为0是关键，同时transitionDuration设0也会有交互体验上的差别
     */
    private Double transitionDuration;
    /**
     * 位置指定，传入{Array}，如[x, y]， 固定位置[x, y]；传入{Function}，如function([x, y]) {return [newX,newY]}，默认显示坐标为输入参数，用户指定的新坐标为输出返回。
     */
    private Object formatter;
    /**
     * 提示框浮层的背景颜色
     */
    private Object backgroundColor;
    /**
     * 提示框浮层的边框颜色
     */
    private Object borderColor;
    /**
     * 提示框浮层的边框宽
     */
    private Integer borderWidth;
    /**
     * 提示框浮层内边距，单位px，默认各方向内边距为5，接受数组分别设定上右下左边距
     */
    private Integer padding;
    /**
     * 文本样式，默认为白色字体
     */
    private TextStyle textStyle;
    /**
     * 额外附加到浮层的 css 样式。如下为浮层添加阴影的示例：
     * extraCssText: 'box-shadow: 0 0 3px rgba(0, 0, 0, 0.3);'
     */
    private String extraCssText;
    /**
     * 坐标轴指示器，坐标轴触发有效
     */
    private AxisPointer axisPointer;

    /**
     * 系列中的数据内容数组。数组项通常为具体的数据项
     */
    private Object data;

    /**
     * 浮层的渲染模式，默认以 'html 即额外的 DOM 节点展示 tooltip；此外还可以设置为 'richText' 表示以富文本的形式渲染，渲染的结果在图表对应的 Canvas 中（目前 SVG 尚未支持富文本），这对于一些没有 DOM 的环境（如微信小程序）有更好的支持。
     */
    private String renderMode;

    public String renderMode() {
        return renderMode;
    }

    public void renderMode(String renderMode) {
        this.renderMode = renderMode;
    }

    public String extraCssText() {
        return this.extraCssText;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }

    public void setEnterable(Boolean enterable) {
        this.enterable = enterable;
    }

    public Boolean getConfine() {
        return confine;
    }

    public void setConfine(Boolean confine) {
        this.confine = confine;
    }

    public Object getBackgroundColor() {
        return backgroundColor;
    }

    public void setBackgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public Object getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Object borderColor) {
        this.borderColor = borderColor;
    }

    public Integer getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
    }

    public Integer getPadding() {
        return padding;
    }

    public void setPadding(Integer padding) {
        this.padding = padding;
    }

    public String getExtraCssText() {
        return extraCssText;
    }

    public void setExtraCssText(String extraCssText) {
        this.extraCssText = extraCssText;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public Tooltip extraCssText(String extraCssText) {
        this.extraCssText = extraCssText;
        return this;
    }

    public Integer padding() {
        return this.padding;
    }

    public Tooltip padding(Integer padding) {
        this.padding = padding;
        return this;
    }

    public Integer borderWidth() {
        return this.borderWidth;
    }

    public Tooltip borderWidth(Integer borderWidth) {
        this.borderWidth = borderWidth;
        return this;
    }

    public Object borderColor() {
        return this.borderColor;
    }

    public Tooltip borderColor(Object borderColor) {
        this.borderColor = borderColor;
        return this;
    }

    public Object backgroundColor() {
        return this.backgroundColor;
    }

    public Tooltip backgroundColor(Object backgroundColor) {
        this.backgroundColor = backgroundColor;
        return this;
    }

    public Boolean confine() {
        return this.confine;
    }

    public Tooltip confine(Boolean confine) {
        this.confine = confine;
        return this;
    }

    public Object data() {
        return data;
    }

    public Tooltip data(Object... data) {
        this.data = data;
        return this;
    }

    public Boolean show() {
        return this.show;
    }

    public Tooltip show(Boolean show) {
        this.show = show;
        return this;
    }

    public Boolean alwaysShowContent() {
        return this.alwaysShowContent;
    }

    public Tooltip alwaysShowContent(Boolean alwaysShowContent) {
        this.alwaysShowContent = alwaysShowContent;
        return this;
    }

    public Boolean getAlwaysShowContent() {
        return alwaysShowContent;
    }

    public void setAlwaysShowContent(Boolean alwaysShowContent) {
        this.alwaysShowContent = alwaysShowContent;
    }

    public String triggerOn() {
        return this.triggerOn;
    }

    public Tooltip triggerOn(String triggerOn) {
        this.triggerOn = triggerOn;
        return this;
    }

    public String getTriggerOn() {
        return triggerOn;
    }

    public void setTriggerOn(String triggerOn) {
        this.triggerOn = triggerOn;
    }

    /**
     * 设置axisPointer值
     *
     * @param axisPointer
     */
    public Tooltip axisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
        return this;
    }

    /**
     * 设置textStyle值
     *
     * @param textStyle
     */
    public Tooltip textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    /**
     * 获取showContent值
     */
    public Boolean showContent() {
        return this.showContent;
    }

    /**
     * 设置showContent值
     *
     * @param showContent
     */
    public Tooltip showContent(Boolean showContent) {
        this.showContent = showContent;
        return this;
    }

    /**
     * 获取trigger值
     */
    public String trigger() {
        return this.trigger;
    }

    /**
     * 设置trigger值
     *
     * @param trigger
     */
    public Tooltip trigger(String trigger) {
        this.trigger = trigger;
        return this;
    }

    /**
     * 获取position值
     */
    public Object position() {
        return this.position;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public Tooltip position(Object position) {
        this.position = position;
        return this;
    }

    /**
     * 获取formatter值
     */
    public Object formatter() {
        return this.formatter;
    }

    /**
     * 设置formatter值
     *
     * @param formatter
     */
    public Tooltip formatter(Object formatter) {
        this.formatter = formatter;
        return this;
    }

    /**
     * 获取showDelay值
     */
    public Integer showDelay() {
        return this.showDelay;
    }

    /**
     * 设置showDelay值
     *
     * @param showDelay
     */
    public Tooltip showDelay(Integer showDelay) {
        this.showDelay = showDelay;
        return this;
    }

    /**
     * 获取hideDelay值
     */
    public Integer hideDelay() {
        return this.hideDelay;
    }

    /**
     * 设置hideDelay值
     *
     * @param hideDelay
     */
    public Tooltip hideDelay(Integer hideDelay) {
        this.hideDelay = hideDelay;
        return this;
    }

    /**
     * 获取transitionDuration值
     */
    public Double transitionDuration() {
        return this.transitionDuration;
    }

    /**
     * 设置transitionDuration值
     *
     * @param transitionDuration
     */
    public Tooltip transitionDuration(Double transitionDuration) {
        this.transitionDuration = transitionDuration;
        return this;
    }

    /**
     * 获取enterable值
     */
    public Boolean enterable() {
        return this.enterable;
    }

    /**
     * 设置enterable值
     *
     * @param enterable
     */
    public Tooltip enterable(Boolean enterable) {
        this.enterable = enterable;
        return this;
    }


    /**
     * 坐标轴指示器，坐标轴触发有效
     */
    public AxisPointer axisPointer() {
        if (this.axisPointer == null) {
            this.axisPointer = new AxisPointer();
        }
        return this.axisPointer;
    }

    /**
     * 文本样式，默认为白色字体
     *
     * @see TextStyle
     */
    public TextStyle textStyle() {
        if (this.textStyle == null) {
            this.textStyle = new TextStyle();
        }
        return this.textStyle;
    }

    /**
     * 获取showContent值
     */
    public Boolean getShowContent() {
        return showContent;
    }

    /**
     * 设置showContent值
     *
     * @param showContent
     */
    public void setShowContent(Boolean showContent) {
        this.showContent = showContent;
    }

    /**
     * 获取trigger值
     */
    public String getTrigger() {
        return trigger;
    }

    /**
     * 设置trigger值
     *
     * @param trigger
     */
    public void setTrigger(String trigger) {
        this.trigger = trigger;
    }

    /**
     * 获取position值
     */
    public Object getPosition() {
        return position;
    }

    /**
     * 设置position值
     *
     * @param position
     */
    public void setPosition(Object position) {
        this.position = position;
    }

    /**
     * 获取formatter值
     */
    public Object getFormatter() {
        return formatter;
    }

    /**
     * 设置formatter值
     *
     * @param formatter
     */
    public void setFormatter(Object formatter) {
        this.formatter = formatter;
    }

    /**
     * 获取showDelay值
     */
    public Integer getShowDelay() {
        return showDelay;
    }

    /**
     * 设置showDelay值
     *
     * @param showDelay
     */
    public void setShowDelay(Integer showDelay) {
        this.showDelay = showDelay;
    }

    /**
     * 获取hideDelay值
     */
    public Integer getHideDelay() {
        return hideDelay;
    }

    /**
     * 设置hideDelay值
     *
     * @param hideDelay
     */
    public void setHideDelay(Integer hideDelay) {
        this.hideDelay = hideDelay;
    }

    /**
     * 获取transitionDuration值
     */
    public Double getTransitionDuration() {
        return transitionDuration;
    }

    /**
     * 设置transitionDuration值
     *
     * @param transitionDuration
     */
    public void setTransitionDuration(Double transitionDuration) {
        this.transitionDuration = transitionDuration;
    }

    /**
     * 获取enterable值
     */
    public Boolean getEnterable() {
        return enterable;
    }

    /**
     * 获取axisPointer值
     */
    public AxisPointer getAxisPointer() {
        return axisPointer;
    }

    /**
     * 设置axisPointer值
     *
     * @param axisPointer
     */
    public void setAxisPointer(AxisPointer axisPointer) {
        this.axisPointer = axisPointer;
    }

    /**
     * 获取textStyle值
     */
    public TextStyle getTextStyle() {
        return textStyle;
    }

    /**
     * 设置textStyle值
     *
     * @param textStyle
     */
    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public String getRenderMode() {
        return renderMode;
    }

    public void setRenderMode(String renderMode) {
        this.renderMode = renderMode;
    }
}
