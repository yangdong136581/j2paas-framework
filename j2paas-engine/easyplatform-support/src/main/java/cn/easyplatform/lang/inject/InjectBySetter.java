/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.lang.inject;

import cn.easyplatform.castor.Castors;
import cn.easyplatform.lang.Lang;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Method;

public class InjectBySetter implements Injecting {

	private static final Logger log = LoggerFactory.getLogger(InjectBySetter.class);

	private Method setter;
	private Class<?> valueType;

	public InjectBySetter(Method setter) {
		this.setter = setter;
		valueType = setter.getParameterTypes()[0];
	}

	public void inject(Object obj, Object value) {
		Object v = null;
		try {
			v = Castors.me().castTo(value, valueType);
			setter.invoke(obj, v);
		} catch (Exception e) {
			if (log.isInfoEnabled())
				log.info("Fail to value by setter", e);
			throw Lang
					.makeThrow(
							"Fail to set '%s'[ %s ] by setter %s.'%s()' because [%s]: %s",
							value, v, setter.getDeclaringClass().getName(),
							setter.getName(), Lang.unwrapThrow(e), Lang
									.unwrapThrow(e).getMessage());
		}
	}

}