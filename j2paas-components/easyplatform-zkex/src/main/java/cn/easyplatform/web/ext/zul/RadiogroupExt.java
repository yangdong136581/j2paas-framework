/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.zul;

import cn.easyplatform.lang.Lang;
import cn.easyplatform.web.ext.*;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.ext.Disable;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

import java.util.Iterator;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RadiogroupExt extends Radiogroup implements ZkExt, Cacheable,
		Reloadable, Assignable, Disable, Queryable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 查询语句,结果column个数可以有1-3个,当1个的时候value和label共用，2个时分别表示value和label，3个时最后一个是图片
	 */
	private Object _query;

	/**
	 * 数据连接的资源id
	 */
	private String _dbId;

	/**
	 * 在列表中是否要缓存本组件
	 */
	private boolean _cache;

	/**
	 * radio的列数
	 */
	private int _cols;

	/**
	 * 允许有一个自定义的选项
	 */
	private String _emptyLabel;

	/**
	 * 自定义选项的值
	 */
	private String _emptyValue = "";

	/**
	 * 过滤表达式
	 */
	private String _filter;

	/**
	 * 子项风格
	 */
	private String _itemStyle;
	/**
	 * 是否必需重新加载
	 */
	private boolean _force;

	@Override
	public boolean isForce() {
		return _force;
	}

	public void setForce(boolean force) {
		this._force = force;
	}
	/**
	 * @return the itemStyle
	 */
	public String getItemStyle() {
		return _itemStyle;
	}

	/**
	 * @param itemStyle
	 *            the itemStyle to set
	 */
	public void setItemStyle(String itemStyle) {
		this._itemStyle = itemStyle;
	}

	public String getFilter() {
		return _filter;
	}

	public void setFilter(String filter) {
		this._filter = filter;
	}

	/**
	 * @return the emptyLabel
	 */
	public String getEmptyLabel() {
		return _emptyLabel;
	}

	/**
	 * 在显示时是否要马上执行查询
	 */
	private boolean _immediate = true;

	/**
	 * 
	 */
	private boolean _disabled;

	/**
	 * @return
	 */
	public boolean isImmediate() {
		return _immediate;
	}

	/**
	 * @param immediate
	 */
	public void setImmediate(boolean immediate) {
		this._immediate = immediate;
	}

	/**
	 * @param emptyLabel
	 *            the emptyLabel to set
	 */
	public void setEmptyLabel(String emptyLabel) {
		this._emptyLabel = emptyLabel;
	}

	/**
	 * @return the emptyValue
	 */
	public String getEmptyValue() {
		return _emptyValue;
	}

	/**
	 * @param emptyValue
	 *            the emptyValue to set
	 */
	public void setEmptyValue(String emptyValue) {
		this._emptyValue = emptyValue;
	}

	public Object getQuery() {
		return _query;
	}

	public void setQuery(Object query) {
		this._query = query;
	}

	public String getDbId() {
		return _dbId;
	}

	public void setDbId(String dbId) {
		this._dbId = dbId;
	}

	public boolean isCache() {
		return _cache;
	}

	public void setCache(boolean cache) {
		this._cache = cache;
	}

	/**
	 * @return the cols
	 */
	public int getCols() {
		return _cols;
	}

	/**
	 * @param cols
	 *            the cols to set
	 */
	public void setCols(int cols) {
		this._cols = cols;
	}

	@Override
	public void reload() {
		Widget ext = (Widget) getAttribute("$proxy");
		ext.reload(this);
	}

	@Override
	public void setValue(Object value) {
		Iterator<Component> itr = queryAll("radio").iterator();
		while (itr.hasNext()) {
			Component c = itr.next();
			Radio radio = (Radio) c;
			radio.setSelected(false);
			if (radio.getValue() != null) {
				if (Lang.equals(radio.getValue(), value))
					radio.setSelected(true);
			} else if (Lang.equals(radio.getLabel(), value))
				radio.setSelected(true);
		}
		Object[] data = (Object[]) getAttribute("data");
		if (data != null) {
			Integer index = (Integer) getAttribute("index");
			if (index != null)
				data[index] = value;
		}
	}

	@Override
	public boolean isDisabled() {
		return _disabled;
	}

	@Override
	public void setDisabled(boolean disabled) {
		if (this._disabled != disabled) {
			Iterator<Component> itr = queryAll("radio").iterator();
			while (itr.hasNext()) {
				Component c = itr.next();
				Radio radio = (Radio) c;
				radio.setDisabled(disabled);
			}
			this._disabled = disabled;
		}
	}

	@Override
	public Object getValue() {
		if (getSelectedItem() != null)
			return getSelectedItem().getValue();
		return null;
	}

	public Object getSelectedValue() {
		return getValue();
	}

	public String getLabel() {
		if (getSelectedItem() != null)
			return getSelectedItem().getLabel();
		return null;
	}
}
