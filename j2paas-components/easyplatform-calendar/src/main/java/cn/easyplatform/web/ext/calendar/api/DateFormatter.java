/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.calendar.api;

import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public interface DateFormatter {
	/**
	 * Returns the caption of the day of week.
	 */
	public String getCaptionByDayOfWeek(Date date, Locale locale, TimeZone timezone);

	/**
	 * Returns the caption of the time of day.
	 */
	public String getCaptionByTimeOfDay(Date date, Locale locale, TimeZone timezone);

	/**
	 * Returns the caption of the date.
	 */
	public String getCaptionByDate(Date date, Locale locale, TimeZone timezone);
	/**
	 * Returns the caption of the date of month.
	 */
	public String getCaptionByDateOfMonth(Date date, Locale locale, TimeZone timezone);
	
	/**
	 * Returns the caption of the popup title.
	 */
	public String getCaptionByPopup(Date date, Locale locale, TimeZone timezone);
	
	/**
	 * Returns the caption of the week number within the current year.
	 */
	public String getCaptionByWeekOfYear(Date date, Locale locale, TimeZone timezone);
}
