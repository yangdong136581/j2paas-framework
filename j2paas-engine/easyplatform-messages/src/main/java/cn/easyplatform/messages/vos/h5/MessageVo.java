/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.messages.vos.h5;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MessageVo implements Serializable, Cloneable {

    /**
     *
     */
    private static final long serialVersionUID = -4823996777516520065L;

    //聊天室
    public final static String TYPE_CHAT = "chat";

    //公告、新闻
    public final static String TYPE_NOTICE = "notice";

    //功能相关的通知，执行功能
    public final static String TYPE_TASK = "bpm";

    //仅通知，没有存储
    public final static String TYPE_MESSAGE = "message";

    public final static int STATE_UNREAD = 0;

    public final static int STATE_READED = 1;

    private String type = TYPE_CHAT;

    private int group;

    private String toUser;

    private String fromUser;

    private long msgid;

    private String text;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public String getToUser() {
        return toUser;
    }

    public void setToUser(String toUser) {
        this.toUser = toUser;
    }

    public String getFromUser() {
        return fromUser;
    }

    public void setFromUser(String fromUser) {
        this.fromUser = fromUser;
    }

    public long getMsgid() {
        return msgid;
    }

    public void setMsgid(long msgid) {
        this.msgid = msgid;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public MessageVo clone() {
        try {
            return (MessageVo) super.clone();
        } catch (CloneNotSupportedException e) {
        }
        return null;
    }
}
