/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.task.support.scripting;

import org.mozilla.javascript.Context;
import org.mozilla.javascript.ImporterTopLevel;
import org.mozilla.javascript.Scriptable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class RhinoScriptable extends ImporterTopLevel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Map<String, Object> map = new HashMap<String, Object>();

	public RhinoScriptable() {
	}

	/**
	 * @param map
	 */
	public RhinoScriptable(Map<String, Object> map, boolean clone) {
		if (clone)
			this.map.putAll(map);
		else
			this.map = map;
	}

	@Override
	public Object get(String name, Scriptable start) {
		if (name.equals("$NOW"))
			return new Date();
		if (map.containsKey(name))
			return Context.javaToJS(map.get(name), this);
		return super.get(name, start);
	}

	@Override
	public void put(String name, Scriptable start, Object value) {
		map.put(name, value);
	}

	@Override
	public void delete(String name) {
		map.remove(name);
		super.delete(name);
	}

	public void clear() {
		for (String name : map.keySet())
			super.delete(name);
		map.clear();
	}

	public void setVariable(String name, Object value) {
		map.put(name, value);
	}

	Set<Map.Entry<String, Object>> entrySet() {
		return map.entrySet();
	}
}
