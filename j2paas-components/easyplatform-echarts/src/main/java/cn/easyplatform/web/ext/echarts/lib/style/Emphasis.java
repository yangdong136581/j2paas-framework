/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.style;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class Emphasis implements Serializable {
    private Boolean show;
    private IconStyle iconStyle;
    private LabelStyle label;
    private ItemStyle itemStyle;
    private LineStyle lineStyle;
    private TextStyle textStyle;

    public Emphasis show(Boolean show) {
        this.show = show;
        return this;
    }

    public TextStyle textStyle() {
        if (textStyle == null)
            textStyle = new TextStyle();
        return textStyle;
    }

    public Emphasis textStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
        return this;
    }

    public LineStyle lineStyle() {
        if (lineStyle == null)
            lineStyle = new LineStyle();
        return lineStyle;
    }

    public Emphasis lineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public Emphasis itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public LabelStyle label() {
        if (label == null)
            label = new LabelStyle();
        return label;
    }

    public Emphasis label(LabelStyle label) {
        this.label = label;
        return this;
    }

    public IconStyle iconStyle() {
        if (iconStyle == null)
            iconStyle = new IconStyle();
        return iconStyle;
    }

    public Emphasis iconStyle(IconStyle iconStyle) {
        this.iconStyle = iconStyle;
        return this;
    }

    public LineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(LineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public IconStyle getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(IconStyle iconStyle) {
        this.iconStyle = iconStyle;
    }

    public LabelStyle getLabel() {
        return label;
    }

    public void setLabel(LabelStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }

    public TextStyle getTextStyle() {
        return textStyle;
    }

    public void setTextStyle(TextStyle textStyle) {
        this.textStyle = textStyle;
    }

    public Boolean getShow() {
        return show;
    }

    public void setShow(Boolean show) {
        this.show = show;
    }
}
