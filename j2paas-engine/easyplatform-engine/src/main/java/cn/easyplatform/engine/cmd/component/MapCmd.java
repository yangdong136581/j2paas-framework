/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.component;

import cn.easyplatform.contexts.ListContext;
import cn.easyplatform.contexts.RecordContext;
import cn.easyplatform.contexts.WorkflowContext;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.dos.Record;
import cn.easyplatform.engine.runtime.datalist.DataListUtils;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.MapRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.messages.vos.component.ListMapVo;
import cn.easyplatform.messages.vos.component.MapVo;
import cn.easyplatform.support.scripting.MappingEngine;
import cn.easyplatform.support.scripting.ScriptEngineFactory;
import cn.easyplatform.type.FieldVo;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.util.MessageUtils;
import cn.easyplatform.util.RuntimeUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class MapCmd extends AbstractCommand<MapRequestMessage> {

	/**
	 * @param req
	 */
	public MapCmd(MapRequestMessage req) {
		super(req);
	}

	@Override
	public IResponseMessage<?> execute(CommandContext cc) {
		WorkflowContext ctx = cc.getWorkflowContext();
		MapVo mv = req.getBody();
		RecordContext target = null;
		if (mv instanceof ListMapVo) {// 目标是列表
			ListMapVo lmv = (ListMapVo) mv;
			ListContext lc = ctx.getList(lmv.getId());
			if (lc == null)
				return MessageUtils.dataListNotFound(lmv.getId());
			target = lc.getRecord(lmv.getKeys());
			if (target == null) {
				if (lc.isCustom()) {
					target = lc.createRecord(lmv.getKeys());
				} else {
					Record record = DataListUtils.getRecord(cc, lc,
							lmv.getKeys());
					target = lc.createRecord(lmv.getKeys(), record);
					target.setParameter("814", 'R');
					target.setParameter("815", Boolean.FALSE);
					lc.appendRecord(target);
				}
			}
		} else {
			target = ctx.getRecord();
		}
		List<FieldDo> tmp = new ArrayList<FieldDo>(mv.getData().length);
		for (FieldVo fv : mv.getData())
			tmp.add(RuntimeUtils.castTo(fv));
		FieldDo[] data = new FieldDo[tmp.size()];
		tmp.toArray(data);
		tmp = null;
		MappingEngine engine = ScriptEngineFactory.createMappingEngine();
		List<String> list = engine.eval(cc, target, data, mv.getMapping());
		Map<String, Object> result = new HashMap<String, Object>();
		for (String name : list)
			result.put(name, target.getValue(name));
		return new SimpleResponseMessage(result);
	}

}
