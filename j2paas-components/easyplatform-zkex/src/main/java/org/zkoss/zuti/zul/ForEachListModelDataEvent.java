/* ForEachListModelDataEvent.java

	Purpose:
		
	Description:
		
	History:
		10:45:00 AM Jan 27, Created by jameschu

Copyright (C) 2015 Potix Corporation. All Rights Reserved.
*/
package org.zkoss.zuti.zul;

import org.zkoss.zul.ListModel;
import org.zkoss.zul.event.ListDataEvent;

/**
 * serializable event in children binding (support list model in children binding)
 * @author jameschu
 * @since 8.0.0
 */
public class ForEachListModelDataEvent extends ListDataEvent implements java.io.Serializable {
	private static final long serialVersionUID = 20150116150500L;

	public ForEachListModelDataEvent(ListModel<?> model, int type, int index0, int index1) {
		super(model, type, index0, index1);
	}

	public ForEachListModelDataEvent(ListDataEvent e) {
		super(e.getModel(), e.getType(), e.getIndex0(), e.getIndex1());
	}

}
