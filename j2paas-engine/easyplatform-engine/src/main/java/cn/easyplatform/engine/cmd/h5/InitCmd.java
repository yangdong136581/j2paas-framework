/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.engine.cmd.h5;

import cn.easyplatform.dao.BizDao;
import cn.easyplatform.dos.FieldDo;
import cn.easyplatform.interceptor.AbstractCommand;
import cn.easyplatform.interceptor.CommandContext;
import cn.easyplatform.messages.request.SimpleRequestMessage;
import cn.easyplatform.messages.response.SimpleResponseMessage;
import cn.easyplatform.type.FieldType;
import cn.easyplatform.type.IResponseMessage;
import cn.easyplatform.messages.vos.h5.MessageVo;

import java.util.ArrayList;
import java.util.List;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class InitCmd extends AbstractCommand<SimpleRequestMessage> {

    /**
     * @param req
     */
    public InitCmd(SimpleRequestMessage req) {
        super(req);
    }

    @Override
    public IResponseMessage<?> execute(CommandContext cc) {
        BizDao dao = cc.getBizDao();
        String type = (String) req.getBody();
        String sql = "select count(*) from sys_notice_detail_info b,sys_notice_info a where a.type=? and b.touser=? and b.status=? and a.id=b.msgid";
        List<FieldDo> params = new ArrayList<FieldDo>();
        params.add(new FieldDo(FieldType.VARCHAR, type));
        params.add(new FieldDo(FieldType.VARCHAR, cc.getUser().getId()));
        params.add(new FieldDo(FieldType.INT, MessageVo.STATE_UNREAD));
        FieldDo fd = dao.selectObject(sql, params);
        if (fd != null) {
            Number num = (Number) fd.getValue();
            return new SimpleResponseMessage(num.intValue());
        }
        return new SimpleResponseMessage();
    }

}
