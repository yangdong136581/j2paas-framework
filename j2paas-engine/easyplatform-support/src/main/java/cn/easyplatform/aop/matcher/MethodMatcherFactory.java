/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.aop.matcher;

import cn.easyplatform.aop.MethodMatcher;

/**
 * 创建MethodMatcher的工厂类
 * 
 * @see cn.easyplatform.aop.MethodMatcher
 * 
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a>
 */
public final class MethodMatcherFactory {

	private MethodMatcherFactory() {
	}

	public static MethodMatcher matcher() {
		return matcher(-1);
	}

	public static MethodMatcher matcher(int mod) {
		return matcher(null, mod);
	}

	public static MethodMatcher matcher(String regex) {
		return matcher(regex, 0);
	}

	public static MethodMatcher matcher(String regex, int mod) {
		return matcher(regex, null, mod);
	}

	public static MethodMatcher matcher(String regex, String ignore, int mod) {
		return new RegexMethodMatcher(regex, ignore, mod);
	}
}
