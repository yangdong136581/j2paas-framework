/* NavigationEvent.java

	Purpose:

	Description:

	History:
		Tue Sep 25 12:12:10 CST 2018, Created by rudyhuang

Copyright (C) 2018 Potix Corporation. All Rights Reserved.
*/
package org.zkoss.zuti;

import org.zkoss.zuti.zul.ForEach;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.util.Template;

import java.util.List;

/**
 * Defines an event that encapsulates changes to a navigation model.
 *
 * @author rudyhuang
 * @since 8.6.0
 */
public interface ForEachRenderer {

    void render(ForEach forEachComp, Component host, Template tm, List<?> list, boolean isUsingListModel);

    void render(ForEach forEachComp, Component host, Template tm, int begin, int end, int step, List<?> list,
                boolean isUsingListModel);
}
