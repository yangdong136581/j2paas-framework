{
    title: {
        text: '人民的名义关系图谱'
    },
    tooltip: {
        show: true,  // 显示节点详细信息
        formatter: "function(params){if(params.data.symbol!=null&&params.data.symbol[0]=='arrow'&&params.data.symbol[1]=='arrow'){return params.name+'&emsp;'+params.data.sourceValue+'<br/>'+params.data.target+'&nbsp;>&nbsp;'+params.data.source+'&emsp;'+params.data.targetValue;}else if(params.data.symbol!=null&&params.data.symbol[0]=='none'&&params.data.symbol[1]=='none'){return '';}else{return params.name+'&emsp;'+params.value;}}" // 悬浮提示文本格式
    },
    legend: {
        show: true  //显示图例
    },
    series: [
        {
            type: 'graph',
            layout: 'force',
            symbolSize: 60,  // 节点基本大小
            focusNodeAdjacency: true, // 高亮目标关系网
            draggable: true,  // 目标可拖拽
            roam: true,  // 画布可拖拽
            autoCurveness: false, // 自动计算节点多线条曲率
            edgeSymbol: ['none', 'arrow'],
            edgeLabel: {
                show: false,  // 显示线条文本
                textStyle: {
                    fontSize: 20  // 线条文本大小
                },
                formatter: "{c}"  // 线条文本格式
            },
            force: {
                repulsion: 2500,  // 节点间斥力因子
                edgeLength: [100, 200]  // 节点距离，[节点间线条数值愈大愈趋向，节点间线条数值愈小愈趋向]
            },
            label: {
                show: true,
                position: 'right', // 节点名称位置
                formatter: '{b}' // 节点名称格式
            },
            emphasis: {
                focus: 'adjacency', // 节点高亮时聚焦类型
                lineStyle: {
                    width: 10 // 线条高亮时宽度
                },
                itemStyle: {
                    // borderColor: 'gray',
                    // borderWidth: 2,
                    shadowColor: 'rgba(0, 0, 0, 0.5)', // 节点高亮时阴影颜色
                    shadowBlur: 15 // 节点高亮时阴影大小
                }
            },
            data: 'data',
            links: [
                {
                    source: '高育良',
                    target: '侯亮平',
                    value: '师生',
                    symbol: ['none', 'none']
                }, {
                    source: '高育良',
                    target: '祁同伟',
                    value: '师生',
                    symbol: ['arrow', 'arrow'],
                    sourceValue:5,
                    targetValue:5
                }, {
                    source: '赵立春',
                    target: '高育良',
                    value: "前领导"
                }, {
                    source: '赵立春',
                    target: '赵瑞龙',
                    value: "父子"
                }, {
                    source: '赵立春',
                    target: '刘新建',
                    value: "前部队下属"
                }, {
                    source: '李达康',
                    target: '赵立春',
                    value: "前任秘书"
                }, {
                    source: '祁同伟',
                    target: '高小琴',
                    value: "情人"
                }, {
                    source: '陈岩石',
                    target: '陈海',
                    value: "父子"
                }, {
                    source: '陆亦可',
                    target: '陈海',
                    value: "属下"
                }, {
                    source: '沙瑞金',
                    target: '陈岩石',
                    value: "故人"
                }, {
                    source: '祁同伟',
                    target: '陈海',
                    value: "师哥"
                }, {
                    source: '祁同伟',
                    target: '侯亮平',
                    value: "师哥"
                }, {
                    source: '侯亮平',
                    target: '陈海',
                    value: "同学，生死朋友"
                }, {
                    source: '高育良',
                    target: '吴惠芬',
                    value: "夫妻"
                }, {
                    source: '陈海',
                    target: '赵东来',
                    value: "朋友"
                }, {
                    source: '高小琴',
                    target: '高小凤',
                    value: "双胞胎"
                }, {
                    source: '赵东来',
                    target: '陆亦可',
                    value: "爱慕"
                }, {
                    source: '祁同伟',
                    target: '程度',
                    value: "领导"
                }, {
                    source: '高育良',
                    target: '高小凤',
                    value: "情人"
                }, {
                    source: '赵瑞龙',
                    target: '祁同伟',
                    value: "勾结"
                }, {
                    source: '李达康',
                    target: '赵东来',
                    value: "领导"
                }, {
                    source: '赵瑞龙',
                    target: '程度',
                    value: "领导"
                }, {
                    source: '沙瑞金',
                    target: '李达康',
                    value: "领导"
                }, {
                    source: '沙瑞金',
                    target: '高育良',
                    value: "领导"
                }, {
                    source: '祁同伟',
                    target: '梁璐',
                    value: "夫妻"
                }, {
                    source: '吴惠芬',
                    target: '梁璐',
                    value: "朋友"
                }, {
                    source: '李达康',
                    target: '欧阳菁',
                    value: "夫妻"
                }, {
                    source: '赵瑞龙',
                    target: '刘新建',
                    value: "洗钱工具"
                }, {
                    source: '赵瑞龙',
                    target: '高小琴',
                    value: "勾结，腐化"
                }, {
                    source: '赵瑞龙',
                    target: '高小凤',
                    value: "腐化"
                }, {
                    source: '吴心怡',
                    target: '陆亦可',
                    value: "母女"
                }, {
                    source: '吴心怡',
                    target: '吴惠芬',
                    value: "姐妹"
                }, {
                    source: '蔡成功',
                    target: '侯亮平',
                    value: "发小"
                }, {
                    source: '蔡成功',
                    target: '欧阳菁',
                    value: "举报"
                }, {
                    source: '欧阳菁',
                    target: '刘新建',
                    value: "举报"
                }, {
                    source: '刘新建',
                    target: '赵瑞龙',
                    value: "举报"
                }, {
                    source: '李达康',
                    target: '丁义珍',
                    value: "领导"
                }, {
                    source: '高小琴',
                    target: '丁义珍',
                    value: "策划出逃"
                }, {
                    source: '祁同伟',
                    target: '丁义珍',
                    value: "勾结"
                }, {
                    source: '赵瑞龙',
                    target: '丁义珍',
                    value: "勾结"
                }
            ]
        }
    ]
}