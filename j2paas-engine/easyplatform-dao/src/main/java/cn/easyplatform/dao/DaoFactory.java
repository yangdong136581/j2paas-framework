/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.dao;

import cn.easyplatform.EasyPlatformWithLabelKeyException;
import cn.easyplatform.dao.impl.DefaultEntityDaoImpl;
import cn.easyplatform.dao.impl.DefaultIdentityDaoImpl;
import cn.easyplatform.dao.impl.DefaultMessageDaoImpl;
import cn.easyplatform.dao.impl.DefaultSeqDaoImpl;
import cn.easyplatform.dao.impl.db2.Db2BizDao;
import cn.easyplatform.dao.impl.derby.DerbyBizDao;
import cn.easyplatform.dao.impl.dm.DmBizDao;
import cn.easyplatform.dao.impl.mysql.MySqlBizDao;
import cn.easyplatform.dao.impl.oracle.OracleBizDao;
import cn.easyplatform.dao.impl.psql.PsqlBizDao;
import cn.easyplatform.dao.impl.sqlserver.SqlServerBizDao;
import cn.easyplatform.dao.impl.sybase.SybaseBizDao;
import cn.easyplatform.dao.utils.DaoUtils;

import javax.sql.DataSource;

import static cn.easyplatform.dao.utils.DaoUtils.*;


/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public final class DaoFactory {

    private DaoFactory() {
    }

    public static EntityDao createEntityDao(DataSource ds) {
        return new DefaultEntityDaoImpl(ds);
    }

    public static MessageDao createMessageDao(DataSource ds) {
        return new DefaultMessageDaoImpl(ds);
    }

    public static BizDao createBizDao(DataSource ds) {
        switch (DaoUtils.getDbType(ds.toString())) {
            case ORACLE:
                return new OracleBizDao(ds);
            case DB2:
                return new Db2BizDao(ds);
            case MYSQL:
                return new MySqlBizDao(ds);
            case SQLSERVER:
                return new SqlServerBizDao(ds);
            case PSQL:
                return new PsqlBizDao(ds);
            case DERBY:
                return new DerbyBizDao(ds);
            case SYBASE:
                return new SybaseBizDao(ds);
            case DM:
                return new DmBizDao(ds);
            default:
                String msg = ds.toString().substring(5);
                msg = msg.substring(0, msg.indexOf(":"));
                throw new EasyPlatformWithLabelKeyException("dao.db.type.not.support",
                        msg);
        }
    }

    public static SeqDao createSeqDao(DataSource ds) {
        return new DefaultSeqDaoImpl(ds);
    }

    public static IdentityDao createIdentityDao(DataSource ds) {
        return new DefaultIdentityDaoImpl(ds);
    }
}
