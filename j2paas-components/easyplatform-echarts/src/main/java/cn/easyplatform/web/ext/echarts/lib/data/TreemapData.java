/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.web.ext.echarts.lib.data;

import cn.easyplatform.web.ext.echarts.lib.style.ItemStyle;

import java.io.Serializable;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
public class TreemapData implements Serializable {

    private static final long serialVersionUID = 7218201600361155091L;

    private Object id;

    private Object name;

    private Object value;

    private Integer visualDimension;

    private Object visualMin;

    private Object visualMax;

    private Object[] color;

    private Object[] colorAlpha;

    private Object[] colorSaturation;

    private Object colorMappingBy;

    private Object visibleMin;

    private Object childrenVisibleMin;

    private ItemStyle label;

    private ItemStyle itemStyle;

    public TreemapData(Object id, Object name, Object value) {
        this.id = id;
        this.name = name;
        this.value = value;
    }

    public TreemapData(Object name, Object value) {
        this.name = name;
        this.value = value;
    }

    public TreemapData(Object name) {
        this.name = name;
    }

    public Object id() {
        return id;
    }

    public TreemapData id(Object id) {
        this.id = id;
        return this;
    }

    public Object name() {
        return name;
    }

    public TreemapData name(Object name) {
        this.name = name;
        return this;
    }

    public Object value() {
        return value;
    }

    public TreemapData value(Object value) {
        this.value = value;
        return this;
    }

    public Integer visualDimension() {
        return visualDimension;
    }

    public TreemapData visualDimension(Integer visualDimension) {
        this.visualDimension = visualDimension;
        return this;
    }

    public Object visualMin() {
        return visualMin;
    }

    public TreemapData visualMin(Object visualMin) {
        this.visualMin = visualMin;
        return this;
    }

    public Object visualMax() {
        return visualMax;
    }

    public TreemapData visualMax(Object visualMax) {
        this.visualMax = visualMax;
        return this;
    }

    public Object color() {
        return color;
    }

    public TreemapData color(Object... color) {
        this.color = color;
        return this;
    }

    public Object colorAlpha() {
        return colorAlpha;
    }

    public TreemapData colorAlpha(Object... colorAlpha) {
        this.colorAlpha = colorAlpha;
        return this;
    }

    public Object colorSaturation() {
        return colorSaturation;
    }

    public TreemapData colorSaturation(Object... colorSaturation) {
        this.colorSaturation = colorSaturation;
        return this;
    }

    public Object colorMappingBy() {
        return colorMappingBy;
    }

    public TreemapData colorMappingBy(Object colorMappingBy) {
        this.colorMappingBy = colorMappingBy;
        return this;
    }

    public Object visibleMin() {
        return visibleMin;
    }

    public TreemapData visibleMin(Object visibleMin) {
        this.visibleMin = visibleMin;
        return this;
    }

    public Object childrenVisibleMin() {
        return childrenVisibleMin;
    }

    public TreemapData childrenVisibleMin(Object childrenVisibleMin) {
        this.childrenVisibleMin = childrenVisibleMin;
        return this;
    }

    public ItemStyle label() {
        if (label == null)
            label = new ItemStyle();
        return label;
    }

    public TreemapData label(ItemStyle label) {
        this.label = label;
        return this;
    }

    public ItemStyle itemStyle() {
        if (itemStyle == null)
            itemStyle = new ItemStyle();
        return itemStyle;
    }

    public TreemapData itemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
        return this;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public Object getName() {
        return name;
    }

    public void setName(Object name) {
        this.name = name;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public Integer getVisualDimension() {
        return visualDimension;
    }

    public void setVisualDimension(Integer visualDimension) {
        this.visualDimension = visualDimension;
    }

    public Object getVisualMin() {
        return visualMin;
    }

    public void setVisualMin(Object visualMin) {
        this.visualMin = visualMin;
    }

    public Object getVisualMax() {
        return visualMax;
    }

    public void setVisualMax(Object visualMax) {
        this.visualMax = visualMax;
    }

    public Object[] getColor() {
        return color;
    }

    public void setColor(Object[] color) {
        this.color = color;
    }

    public Object[] getColorAlpha() {
        return colorAlpha;
    }

    public void setColorAlpha(Object[] colorAlpha) {
        this.colorAlpha = colorAlpha;
    }

    public Object[] getColorSaturation() {
        return colorSaturation;
    }

    public void setColorSaturation(Object[] colorSaturation) {
        this.colorSaturation = colorSaturation;
    }

    public Object getColorMappingBy() {
        return colorMappingBy;
    }

    public void setColorMappingBy(Object colorMappingBy) {
        this.colorMappingBy = colorMappingBy;
    }

    public Object getVisibleMin() {
        return visibleMin;
    }

    public void setVisibleMin(Object visibleMin) {
        this.visibleMin = visibleMin;
    }

    public Object getChildrenVisibleMin() {
        return childrenVisibleMin;
    }

    public void setChildrenVisibleMin(Object childrenVisibleMin) {
        this.childrenVisibleMin = childrenVisibleMin;
    }

    public ItemStyle getLabel() {
        return label;
    }

    public void setLabel(ItemStyle label) {
        this.label = label;
    }

    public ItemStyle getItemStyle() {
        return itemStyle;
    }

    public void setItemStyle(ItemStyle itemStyle) {
        this.itemStyle = itemStyle;
    }
}
