/**
 * Copyright 2019 吉鼎科技.

 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package cn.easyplatform.entities.beans.project;

import cn.easyplatform.entities.BaseEntity;
import cn.easyplatform.entities.helper.ModeTypeAdapter;
import cn.easyplatform.entities.helper.PropertyAdapter;
import cn.easyplatform.type.ModeType;

import javax.xml.bind.annotation.*;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.List;
import java.util.Map;

/**
 * @author <a href="mailto:davidchen@epclouds.com">littleDog</a> <br/>
 * @since 2.0.0 <br/>
 */
@XmlType(propOrder = {"appContext", "bizDb", "entityTableName", "languages",
        "license", "identityDb", "mode", "services", "commands", "devices", "portlets", "theme", "licenseServer",
        "properties"})
@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name = "project")
public class ProjectBean extends BaseEntity {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * 平台运行时版本号
     */
    @XmlAttribute
    private String version;

    /**
     * 项目对应的app名称，例如?app=wz中的wz
     */
    @XmlElement(required = true)
    private String appContext;

    /**
     * 对应的业务数据库
     */
    @XmlElement(required = true)
    private String bizDb;

    /**
     * 对应的参数表名称
     */
    @XmlElement(required = true)
    private String entityTableName = "ep_entities_info";

    /**
     * 支持的语言集合，第1个表是系统默认，例如：zh_CN,zh_TW,en_US
     */
    @XmlElement(required = true)
    private String languages;

    /**
     * 授权信息
     */
    @XmlElement(required = true)
    private String license;

    /**
     * 用户表所在的数据源id,同一个用户表可以服务多个项目的安全验证
     */
    @XmlElement(required = true)
    private String identityDb;

    /**
     * 风格
     */
    @XmlElement
    private String theme;

    /**
     * 运行查式
     */
    @XmlElement
    @XmlJavaTypeAdapter(value = ModeTypeAdapter.class)
    private ModeType mode = ModeType.DEVELOP;

    /**
     * 许可证服务器
     */
    @XmlElement
    private String licenseServer;

    @XmlElement(name = "properties")
    @XmlJavaTypeAdapter(value = PropertyAdapter.class)
    private Map<String, String> properties;

    /**
     * 支持的客户端
     */
    @XmlElement(name = "device")
    private List<DeviceMapBean> devices;

    /**
     * 门户接口,例如?app=wz&porlet=test中的test
     */
    @XmlElementWrapper(name = "portlets")
    @XmlElement(name = "portlet")
    private List<PortletBean> portlets;

    /**
     * 自定义服务
     */
    @XmlElementWrapper(name = "services")
    @XmlElement(name = "service")
    private List<ServiceBean> services;

    /**
     * 自定义函数
     */
    @XmlElementWrapper(name = "commands")
    @XmlElement(name = "command")
    private List<CommandBean> commands;

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return the appContext
     */
    public String getAppContext() {
        return appContext;
    }

    /**
     * @param appContext the appContext to set
     */
    public void setAppContext(String appContext) {
        this.appContext = appContext;
    }

    /**
     * @return the bizDb
     */
    public String getBizDb() {
        return bizDb;
    }

    /**
     * @param bizDb the bizDb to set
     */
    public void setBizDb(String bizDb) {
        this.bizDb = bizDb;
    }

    /**
     * @return the entityTableName
     */
    public String getEntityTableName() {
        return entityTableName;
    }

    /**
     * @param entityTableName the entityTableName to set
     */
    public void setEntityTableName(String entityTableName) {
        this.entityTableName = entityTableName;
    }

    /**
     * @return the languages
     */
    public String getLanguages() {
        return languages;
    }

    /**
     * @param languages the languages to set
     */
    public void setLanguages(String languages) {
        this.languages = languages;
    }

    /**
     * @return the license
     */
    public String getLicense() {
        return license;
    }

    /**
     * @param license the license to set
     */
    public void setLicense(String license) {
        this.license = license;
    }

    /**
     * @return the identityDb
     */
    public String getIdentityDb() {
        return identityDb;
    }

    /**
     * @param identityDb the identityDb to set
     */
    public void setIdentityDb(String identityDb) {
        this.identityDb = identityDb;
    }

    /**
     * @return the mode
     */
    public ModeType getMode() {
        return mode;
    }

    /**
     * @param mode the model to set
     */
    public void setMode(ModeType mode) {
        this.mode = mode;
    }

    /**
     * @return the devices
     */
    public List<DeviceMapBean> getDevices() {
        return devices;
    }

    /**
     * @param devices the devices to set
     */
    public void setDevices(List<DeviceMapBean> devices) {
        this.devices = devices;
    }

    /**
     * @return the properties
     */
    public Map<String, String> getProperties() {
        return properties;
    }

    /**
     * @param properties the properties to set
     */
    public void setProperties(Map<String, String> properties) {
        this.properties = properties;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(String theme) {
        this.theme = theme;
    }

    /**
     * @return the portlets
     */
    public List<PortletBean> getPortlets() {
        return portlets;
    }

    /**
     * @param portlets the portlets to set
     */
    public void setPortlets(List<PortletBean> portlets) {
        this.portlets = portlets;
    }

    public List<ServiceBean> getServices() {
        return services;
    }

    public void setServices(List<ServiceBean> services) {
        this.services = services;
    }

    public List<CommandBean> getCommands() {
        return commands;
    }

    public void setCommands(List<CommandBean> commands) {
        this.commands = commands;
    }

    public String getLicenseServer() {
        return licenseServer;
    }

    public void setLicenseServer(String licenseServer) {
        this.licenseServer = licenseServer;
    }
}
